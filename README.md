# README #

### SetUp Application
* goto project folder
* Run 'npm install'
* Then Run 'npm start'
* Goto http://localhost:5000

## For Heroku Application
* goto https://floating-peak-21778.herokuapp.com/

### APIs
## /nexmo/sendsms
* Used to Send Sms
* type : Get
* Library: Nexmo

## /nexmo/voicecall
* Used to make a call
* type : Get
* Library: Nexmo

## /nexmo/verifynumber
* Used to Verify Given Number
* type : Get
* Library: Nexmo

## /nexmo/twiliomakeacall
* Used to Make a Call
* type : Get
* Library: twiliom 
* Note: I can't Test This API and This API is only be available on Heroku