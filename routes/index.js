var express = require('express');
var router = express.Router();
var request = require('request');
var Nexmo = require('nexmo'); 
var nexmo = new Nexmo({apiKey: 'faf8aecd', apiSecret: 'a51404ebd2a20be7'}, {debug: true});

var twilio = require('twilio');
var twilioRest = twilio('ACf91069cc7a988d677432e22fed4f70d9', 'ca55b8c795c10b49d747433e18220529');

/* GET users listing. */
router.get('/sendsms', function(req, res, next) {

  nexmo.message.sendSms('NEXMO', '923160333944', 'Welcome to Nexmo', function(err, result){
    if(err) {
      console.log(err);
    } 
    console.log(result);
    var decodedResponse = result;

    console.log('You sent ' + decodedResponse['message-count'] + ' messages.\n');

    decodedResponse['messages'].forEach(function(message) {
        if (message['status'] === "0") {
          console.log('Success ' + message['message-id']);
          res.status(200).jsonp('Message Sent Successfully');
        }
        else {
          console.log('Error ' + message['status']  + ' ' +  message['error-text']);
          res.status(500).jsonp('Error Occured while Sending the SMS');
        }
    });
  });
});


router.get('/voicecall', function(req, res, next) {

  nexmo.voice.call('923160333944','./voicexml.vxml', function(err, result){
    if(err) {
      console.log(err);
      res.status(500).jsonp(err);
    } 
    console.log(result);
    res.status(200).jsonp(result);
  });
});

router.get('/verifynumber', function(req, res, next) {

  nexmo.verify.request(
    {
      number:'923332094049',
      brand:'NexmoAPIapp'
    },function(err, result){
      if(err) {
        console.log(err);
        res.status(500).jsonp(err);
      } 
      console.log(result);
      res.status(200).jsonp(result);
    });
});

router.get('/twiliomakeacall', function(req, res, next) {

  var url = 'https://floating-peak-21778.herokuapp.com/nexmo/outbound';
  twilioRest.makeCall({
      to: '+12015142299',
      from: '+12015142299',
      url: url
  }, function(err, message) {
      console.log(err);
      if (err) {
          res.status(500).send(err);
      } else {
          res.status(200).send({
              message: 'Thank you! We will be calling you shortly.'
          });
      }
  });
});

// Return TwiML instuctions for the outbound call
router.post('/outbound', function(req, res) {
    // We could use twilio.TwimlResponse, but Jade works too - here's how
    // we would render a TwiML (XML) response using Jade
    res.type('text/xml');
    res.render('outbound');
});



module.exports = router;
